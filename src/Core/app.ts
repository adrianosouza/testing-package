import chalk from 'chalk'
import { v4 as uid } from 'uuid'

export default (name: string) => {
    const OK = chalk.greenBright('Ok')
    const FAIL = chalk.redBright('Fail')
    const USER = chalk.blueBright(name)
    const ID = chalk.yellow(uid())
    const UXTT = Date.now()
    const DATE = new Date(UXTT)
    const UXTT_STRING = UXTT.toString()
    const RULE = Number(UXTT_STRING[UXTT_STRING.length - 1])

    console.log(`Starting System for ${USER} ${DATE}`)
    console.log(`Session: ${ID}`)
    RULE % 2 === 0
        ? console.log(`Connection ${OK}\n SECRET IS: ABC!@#`)
        : console.log(`Connection ${FAIL}`, 'Sorry you not connected')

    return RULE / 2 === 0
}