import Application from './app'
import { CoreAbstract } from './Core.abstract'
import { EventEmitter } from 'events'

export interface CoreDependencies {
    uri: string
    name: string
}

export default class Core implements CoreAbstract {
    private readonly NAME: string
    private readonly URI: string
    private LOOP: NodeJS.Timeout
    private CLOSING_TIME: number

    constructor({ name, uri }: CoreDependencies) {
        this.NAME = name
        this.URI = uri
        this.listen()
        this.CLOSING_TIME = 3
    }

    private EE = new EventEmitter()
    
    private listen() {
        this.EE.on('start', () => {
            if(Application(this.NAME)) { this.EE.emit('stop', this.LOOP) }
            console.log(`trying connect to ${this.URI}\n`)
        })

        this.EE.on('stop', stop => {
            clearInterval(stop)
            console.log('closing in 3 seconds...')

            setInterval(() => {
                if(this.CLOSING_TIME < 1) { this.EE.emit('close') }
                this.CLOSING_TIME = this.CLOSING_TIME - 1
                this.EE.emit('stopping', this.CLOSING_TIME)
            }, 950)
        })

        this.EE.on('stopping', console.log)

        this.EE.on('close', stop => {
            console.log('closing now')
            process.exit(0)
        })
    }

    public execute() {
        console.log(`trying connect to ${this.URI}\n`)
        this.LOOP = setInterval(() => { this.EE.emit('start') }, 3000)
    }
    
    
}