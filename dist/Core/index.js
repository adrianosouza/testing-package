"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const events_1 = require("events");
class Core {
    constructor({ name, uri }) {
        this.EE = new events_1.EventEmitter();
        this.NAME = name;
        this.URI = uri;
        this.listen();
        this.CLOSING_TIME = 3;
    }
    listen() {
        this.EE.on('start', () => {
            if (app_1.default(this.NAME)) {
                this.EE.emit('stop', this.LOOP);
            }
            console.log(`trying connect to ${this.URI}\n`);
        });
        this.EE.on('stop', stop => {
            clearInterval(stop);
            console.log('closing in 3 seconds...');
            setInterval(() => {
                if (this.CLOSING_TIME < 1) {
                    this.EE.emit('close');
                }
                this.CLOSING_TIME = this.CLOSING_TIME - 1;
                this.EE.emit('stopping', this.CLOSING_TIME);
            }, 950);
        });
        this.EE.on('stopping', console.log);
        this.EE.on('close', stop => {
            console.log('closing now');
            process.exit(0);
        });
    }
    execute() {
        console.log(`trying connect to ${this.URI}\n`);
        this.LOOP = setInterval(() => { this.EE.emit('start'); }, 3000);
    }
}
exports.default = Core;
//# sourceMappingURL=index.js.map