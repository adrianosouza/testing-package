import { CoreAbstract } from './Core.abstract';
export interface CoreDependencies {
    uri: string;
    name: string;
}
export default class Core implements CoreAbstract {
    private readonly NAME;
    private readonly URI;
    private LOOP;
    private CLOSING_TIME;
    constructor({ name, uri }: CoreDependencies);
    private EE;
    private listen;
    execute(): void;
}
