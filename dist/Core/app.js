"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = __importDefault(require("chalk"));
const uuid_1 = require("uuid");
exports.default = (name) => {
    const OK = chalk_1.default.greenBright('Ok');
    const FAIL = chalk_1.default.redBright('Fail');
    const USER = chalk_1.default.blueBright(name);
    const ID = chalk_1.default.yellow(uuid_1.v4());
    const UXTT = Date.now();
    const DATE = new Date(UXTT);
    const UXTT_STRING = UXTT.toString();
    const RULE = Number(UXTT_STRING[UXTT_STRING.length - 1]);
    console.log(`Starting System for ${USER} ${DATE}`);
    console.log(`Session: ${ID}`);
    RULE % 2 === 0
        ? console.log(`Connection ${OK}\n SECRET IS: ABC!@#`)
        : console.log(`Connection ${FAIL}`, 'Sorry you not connected');
    return RULE / 2 === 0;
};
//# sourceMappingURL=app.js.map